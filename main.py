import getch


def print_key(key, keycode):
    print("Key pressed: " + key)
    print("Keycode: " + str(keycode))


def input():
    key = getch.getch()
    keycode = ord(key)
    print_key(key, keycode)

    if keycode == 27:  # ESC
        return 1

    return 0


def game_loop():  # Main game loop
    while True:
        if (input()):
            break
    print("Exiting...\n")


    # Run the main game loop
game_loop()
