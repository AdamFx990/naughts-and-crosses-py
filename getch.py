# FILE: getch.py
# DESC: Get character class that supports Windows as well as UX.


class _Getch:
    def __init__(self):
        try:
            self.impl = _GetchWin()
        except ImportError:
            self.impl = _GetchUx()

    def __call__(self): return self.impl()


class _GetchUx:
    def __init__(self):
        import tty
        import sys

    def __call__(self):
        import sys
        import tty
        import termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWin:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


getch = _Getch()
